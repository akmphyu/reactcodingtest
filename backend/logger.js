import winston from "winston";
const { combine, timestamp, json } = winston.format;

const logger = winston.createLogger({
  defaultMeta: { component: "user-service" },
  format: combine(
    timestamp({
      format: "YYYY-MM-DD HH:mm:ss",
    }),
    json()
  ),
  transports: [
    new winston.transports.Console(),
    new winston.transports.File({ filename: "combined.log", level: "info" }),
    new winston.transports.File({ filename: "error.log", level: "error" }),
  ],
});

export default logger;
