import mongoose from "mongoose";

const Schema = mongoose.Schema;
const ContactSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  phone: {
    type: Number,
    required: true,
  },
  category: {
    type: String,
    required: true,
  },
  message: {
    type: String,
  },
});

export default mongoose.model("Contacts", ContactSchema);
