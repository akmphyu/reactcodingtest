import mongoose from "mongoose";

const Schema = mongoose.Schema;
const PsgGrantSchema = new Schema({
  
  name: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  phone: {
    type: Number,
    required: true,
  },
  vehicle: {
    type: String,
    required: true,
  },
  date: {
    type: Date,
    default: Date.now,
  },
});

export default mongoose.model("PsgGrants", PsgGrantSchema);
