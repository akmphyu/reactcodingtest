//importing
import express from "express";
import mongoose from "mongoose";
import cors from "cors";
import Contacts from "./model/contact.js";
import PsgGrants from "./model/psg-grant.js";
import rateLimit from "express-rate-limit";
import logger from "./logger.js";

//app config
const app = express();
const port = process.env.PORT || 9000;
const MAX_LIMIT = 1;
const PER_MIN = 15;
const limiter = (minute, max) =>
  rateLimit({
    windowMs: minute * 60 * 1000, // per min
    max: max, // limit each IP to specific requests per windowMs
    message: `You have requested ${max} in ${minute} minutes. Please try again later`,
    handler: function (req, res) {
      logger.error("Request rejected");
      res.send(
        `You have requested ${max} in ${minute} minutes. Please try again later`
      );
    },
  });

//middle ware
app.use(express.json());
app.use(cors());
app.use(limiter(PER_MIN, MAX_LIMIT));

//DB config
const connection_url =
  "mongodb+srv://admin:qoHttT15hDpRn3tY@cluster0.tunth.mongodb.net/cartrackDB?retryWrites=true&w=majority";
mongoose.connect(connection_url, {
  useUnifiedTopology: true,
});
//routes
app.post("/contact-us/new", limiter(PER_MIN, MAX_LIMIT), function (req, res) {
  const newContact = req.body;
  Contacts.create(newContact, (err, data) => {
    if (err) {
      logger.error("Contact us form submission failed");
      res.status(500).send(err);
    } else {
      res.status(201).send(data);
      logger.info("Contact us from submitted");
    }
  });
});
app.get("/contact-us/", function (req, res) {
  Contacts.find((err, data) => {
    if (err) {
      res.status(500).send(err);
    } else {
      res.status(200).send(data);
    }
  });
});

app.post("/psg-grant/new", limiter(PER_MIN, MAX_LIMIT), function (req, res) {
  const newPsgGrant = req.body;
  PsgGrants.create(newPsgGrant, (err, data) => {
    if (err) {
      logger.error("PSG GRANT submission failed");
      res.status(500).send(err);
    } else {
      res.status(201).send(data);
      logger.info("PSF GRANT created");
    }
  });
});
app.get("/psg-grant/", function (req, res) {
  PsgGrants.find((err, data) => {
    if (err) {
      res.status(500).send(err);
    } else {
      res.status(200).send(data);
    }
  });
});
app.get("/", limiter(PER_MIN, MAX_LIMIT), (req, res) => {
  res.status(201).send("hello");
});
//listen
app.listen(port, () => console.log(`Listening on localhost:${port}`));
