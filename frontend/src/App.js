import "./App.css";
import React from "react";
import Header from "./components/Header";
import Home from "./pages/Home";
import PsgGrant from "./pages/PsgGrant";
import ContactUs from "./pages/ContactUs";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

function App() {
  return (
    <Router>
      <div className="App">
        <Switch>
          <Route exact path="/">
            <Header />
            <Home />
          </Route>
          <Route path="/contact-us">
            <Header />
            <ContactUs />
          </Route>

          <Route path="/psg-grant">
            <Header />
            <PsgGrant />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
