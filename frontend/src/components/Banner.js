import React from "react";
import background from "../images/background2.jpg";
import "../css/Banner.css";

function Banner() {
  return (
    <header
      className="banner"
      style={{
        backgroundSize: "cover",
        backgroundImage: `url(${background})`,
        backgroundPosition: "center center",
      }}
    >
      <div className="banner__contents">
        <h4>FLEET MANAGEMENT SOFTWARE</h4>
        <h1 className="banner__title">
          Everything you need for your fleet, now all in one place
        </h1>
        <h3>
          Cartrack’s easy-to-use platform empowers you to streamline your entire
          operation from job management to cost analysis with powerful
          automation and actionable insights.
        </h3>
        <img
          className="app_image"
          src="https://assets.website-files.com/607466742455cd87eb6eb31d/607466742455cdf6d56eb51c_Homepage%20(1)%20(1).svg"
        />
        <div className="banner__buttons">
          <button className="banner__button">Chat with us today</button>
          <a className="banner__link">Learn more</a>
        </div>
      </div>
      <div className="banner--fadeBottom"></div>
    </header>
  );
}

export default Banner;
