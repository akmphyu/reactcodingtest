import React from "react";
import "../css/Card.css";
import QuestionAnswerIcon from "@mui/icons-material/QuestionAnswer";

function Card({ title, description }) {
  return (
    <div className="card">
      <QuestionAnswerIcon />
      <h2>{title}</h2>
      <h4>{description}</h4>
    </div>
  );
}

export default Card;
