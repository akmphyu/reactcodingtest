import React from "react";
import "../css/Customer.css";

function Customer() {
  return (
    <div className="customer">
      <h1>
        We are the global leader in fleet management and connected vehicles
      </h1>
      <div className="customers">
        <div className="customer-info">
          <h1>58 billion +</h1>
          <h4>data points processed monthly</h4>
        </div>
        <div className="customer-info">
          <h1>1,400,000 +</h1>
          <h4>active subscribers across 23 countries</h4>
        </div>
        <div className="customer-info">
          <h1>76,000+</h1>
          <h4>business trust us</h4>
        </div>
      </div>
      <img className="customer-image" src="https://assets.website-files.com/607466742455cd87eb6eb31d/608245df2b44afa18c5d16ee_Customer-Logo_SG_FINAL-min.jpg" />
    </div>
  );
}

export default Customer;
