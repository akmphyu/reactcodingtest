import React from "react";
import "../css/Footer.css";
import mediumLogo from "../images/medium_logo.png";
import smallLogo from "../images/small_logo.png";
import GooglePlay from "../images/googleplay.png";
import AppGallery from "../images/appgallery.png";
import AppStore from "../images/appstore.png";
function Footer() {
  return (
    <div className="footer">
 
      <div className="footer_row">
        <div className="footer_column">
          <h1>Login</h1>
          <h1>Home</h1>
          <h1>Overview</h1>
          <h1>Get a demo</h1>
          <h1>Contact Sales</h1>
          <h1>Contact Us</h1>
          <h1>Careers</h1>
        </div>
        <div className="footer_column">
          <h1>Industries</h1>
          <p>Cold Chain Logistics</p>
          <p>Construction</p>
          <p>Rental & Leasing</p>
          <p>Taxi</p>
          <p>Transportation & Logistics</p>
        </div>
        <div className="footer_column">
          <h1>Teams</h1>
          <p>Admin</p>
          <p>Customers</p>
          <p>Job Deispachers</p>
          <p>Drivers</p>
          <p>Managers</p>
        </div>
        <div className="footer_column">
          <h1>Work Optimisation</h1>
          <p>Actionable Insights</p>
          <p>Easy Admin</p>
          <p>Task Management</p>
          <p>Team Management</p>
        </div>
        <div className="footer_column">
          <h1>Telematics & Real-Time Visibility</h1>
          <p>Trailer Tracking</p>
          <p>Fuel Monitoring</p>
          <p>Temperature Monitoring</p>
          <p>GPS Tracking</p>
          <p>Equipment Monitoring/Asset Management</p>
          <p>Engine & Maintenance</p>
          <p>Specialised Sensors</p>
        </div>
      </div>
      <div className="footer_divider"></div>
      <div className="footer_bottom">
        <div className="footer_left">
          <img src={mediumLogo} />
          <p>Terms and Condition</p>
          <p>Disclaimer</p>
          <p>Data Protection Policy</p>
        </div>
        <div className="footer_right">
          <img src={smallLogo} />
          <img src={AppStore} />
          <img src={GooglePlay} />
          <img src={AppGallery} />
        </div>
      </div>
    </div>
  );
}

export default Footer;
