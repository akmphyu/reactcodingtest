import React, { useEffect, useState } from "react";
import logo from "../images/logo.png";
import "../css/Header.css";
import { Link } from "react-router-dom";

function Header() {
  const [show, handleShow] = useState(false);
  useEffect(() => {
    window.addEventListener("scroll", () => {
      if (window.scrollY > 100) {
        handleShow(true);
      } else {
        handleShow(false);
      }
    });
    return () => {
      window.removeEventListener("scroll");
    };
  }, []);
  return (
    <div className={`header ${show && "header_white"}`}>
      <div className="header_left">
        <Link to="/">
          <img className="header_logo" src={logo} alt="Logo" />
        </Link>

        <div className="header_tab"> Solutions</div>
        <div className="header_tab">Platform</div>
        <Link to="/psg-grant" className="text-link">
          <div className="header_tab">PSG Grant</div>
        </Link>
        <Link to="/contact-us" className="text-link">
          <div className="header_tab">Contact Us</div>
        </Link>
      </div>
      <div className="header_right">
        <button className="contact_btn">
          {" "}
          <Link to="/contact-us" className="text-link">
            Contact Sales
          </Link>{" "}
        </button>

        <button className="login_btn"> Login </button>
      </div>
    </div>
  );
}

export default Header;
