import React, { Component } from "react";
import Footer from "../components/Footer";
import "../css/ContactUs.css";
import axios from "axios";

export class ContactUs extends Component {
  constructor(props) {
    super(props);
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handlePhoneChange = this.handlePhoneChange.bind(this);
    this.handleCategoryChange = this.handleCategoryChange.bind(this);
    this.handleMessageChange = this.handleMessageChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

    this.state = {
      name: "",
      email: "",
      phone: "",
      category: "0",
      message: "",
    };
  }
  handleNameChange = (e) => {
    this.setState({ name: e.target.value });
  };
  handleEmailChange = (e) => {
    this.setState({ email: e.target.value });
  };
  handlePhoneChange = (e) => {
    this.setState({ phone: e.target.value });
  };
  handleCategoryChange = (e) => {
    this.setState({ category: e.target.value });
  };
  handleMessageChange = (e) => {
    this.setState({ message: e.target.value });
  };
  handleSubmit = (e) => {
    e.preventDefault();
    const obj = {
      name: this.state.name,
      email: this.state.email,
      phone: this.state.phone,
      category: this.state.category,
      message: this.state.message,
    };
    axios
      .post("http://localhost:9000/contact-us/new", obj)
      .then((res) => {
        if (res.status == "201") {
          alert("Successfully submitted");
        }
        const data = res.data;
        if (res.status == "200") {
          alert(data);
        }
      })
      .catch((error) => {
        if (error.response) {
          alert(error.response.data);
        }
        if (error.status === "500") {
          alert("You have request too much. Please try again later");
        }
      });

    this.setState({
      name: "",
      email: "",
      phone: "",
      category: "0",
      message: "",
    });
  };
  render() {
    return (
      <div className="contact-us">
        <div className="contact_body">
          <div className="contact_content">
            <div className="contact_info">
              <h1>Emergency Contact</h1>
              <p>
                <span>Tel.: </span> +65 6789 9990
              </p>
            </div>
            <div className="contact_info">
              <h1>Sales</h1>
              <p>
                <span>Tel.: </span> +65 6255 4151
              </p>
              <p>
                <span>Email: </span> info@cartrack.sg
              </p>
            </div>
            <div className="contact_info">
              <h1>Customer Care & Support</h1>
              <p>
                <span>Client Service's Number: </span> +65 6255 4151
              </p>
              <p>
                <span>Email: </span> hello.sg@cartrack.com
              </p>
            </div>
            <div className="contact_info">
              <h1>Address</h1>
              <p>
                2 Aljunied Ave 1, #06-11, Framework Building 2, Singapore 389977
              </p>
              <span>Operating Hours:</span>
              <p>Weekdays: 9:00AM - 6:00PM</p>
              <p>Emergency hotline: 24/7</p>
            </div>
          </div>

          <div className="contact_form">
            <h2>Get a free demo</h2>
            <form onSubmit={this.handleSubmit}>
              <div className="form_input">
                <label>Name</label>
                <input
                  type="text"
                  value={this.state.name}
                  onChange={this.handleNameChange}
                  placeholder="Your first and last name"
                />
              </div>
              <div className="form_input">
                <label>Email Address</label>
                <input
                  type="email"
                  value={this.state.email}
                  onChange={this.handleEmailChange}
                  placeholder="Your email address"
                />
              </div>
              <div className="form_input">
                <label>Phone Number</label>
                <input
                  type="text"
                  value={this.state.phone}
                  onChange={this.handlePhoneChange}
                  placeholder="Your mobile phone number"
                />
              </div>
              <div className="form_input">
                <label>I need help with</label>
                <select
                  value={this.state.category}
                  onChange={this.handleCategoryChange}
                >
                  <option value="0">Select one..</option>
                  <option value="Get a demo">Get a demo</option>
                  <option value="Billing & Finance">Billing & Finance</option>
                  <option value="Careers">Careers</option>
                  <option value="Customer Care">Customer Care</option>
                  <option value="Sales">Sales</option>
                </select>
              </div>
              <div className="form_input">
                <label>Message</label>
                <textarea
                  placeholder="Your Message"
                  onChange={this.handleMessageChange}
                />
              </div>
              <button className="submit-btn" type="submit">
                Submit
              </button>
            </form>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default ContactUs;
