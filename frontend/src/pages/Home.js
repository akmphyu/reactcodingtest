import Card from "../components/Card";
import React, { useEffect } from "react";
import "../css/Home.css";
import Banner from "../components/Banner";
import Customer from "../components/Customer";
import Footer from "../components/Footer";
import axios from "axios";

function Home() {
  useEffect(() => {
    axios
      .get("http://localhost:9000/")
      .then((res) => {
        console.log(res);
        if (res.status == "200") {
          alert(res.data);
        }
      })
      .catch((error) => {
        console.log("Error=>", error);
      });
  }, []);
  return (
    <div className="home">
      <Banner />
      <div className="home-card">
        <Card
          title="Task management made easy"
          description="Seamlessly assign jobs to your entire fleet and empower your team to successfully complete jobs with our easy-to-use app. Exceed customer expectations, grow your operations and boost efficiency."
        />

        <Card
          title="Automate your fleet admin"
          description="Forget about spreadsheets and outdated software. Keep your entire operation moving smoothly and fully understand your costs from one platform with only a few clicks."
        />
        <Card
          title="Improve safety with AI-powered cameras"
          description="Protect your drivers with AI-powered cameras that preventatively alert them in real-time of any avoidable high-risk safety events, so they can adapt their behaviour to reduce accidents and save lives."
        />
      </div>
      <Customer />
      <Footer />
    </div>
  );
}

export default Home;
