import React, { Component } from "react";
import Footer from "../components/Footer";
import "../css/PsgGrant.css";
import axios from "axios";

export class PsgGrant extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      email: "",
      phone: "",
      vehicle: "0",
      alert: "",
    };
  }
  handleNameChange = (e) => {
    this.setState({ name: e.target.value });
  };
  handleEmailChange = (e) => {
    this.setState({ email: e.target.value });
  };
  handlePhoneChange = (e) => {
    this.setState({ phone: e.target.value });
  };
  handleVehicleChange = (e) => {
    this.setState({ vehicle: e.target.value });
  };
  handleSubmit = (e) => {
    e.preventDefault();
    const obj = {
      name: this.state.name,
      email: this.state.email,
      phone: this.state.phone,
      vehicle: this.state.vehicle,
    };
    axios
      .post("http://localhost:9000/psg-grant/new", obj)
      .then((res) => {
        if (res.status == "201") {
          alert("Successfully submitted");
        }
        const data = res.data;
        if (res.status == "200") {
          alert(data);
        }
      })
      .catch((error) => {
        if (error.response.data) {
          alert(error.response.data);
        }
        if (error.status === "500") {
          alert("You have request too much. Please try again later");
        }
      });

    this.setState({
      name: "",
      email: "",
      phone: "",
      vehicle: "0",
    });
  };
  render() {
    return (
      <div className="psg_grant">
        {this.state.alert && <p>this.state.alert</p>}
        <div className="psg_body">
          <div className="psg_content">
            <h1>Up to 80% off with the PSG Grant</h1>
            <p>
              Our Fleet Management and Workforce Optimisation solutions are
              endorsed by Infocomm Media Development Adgency (IMDA) & Enterprise
              Singapore (ESG) for the Productivity Solutions Grant (PSG), which
              supports companies keen on adopting IT solutions and equipment to
              enhance business processes. Find out more here.
            </p>
            <p>
              To find out if your company is eligible for the PSG scheme, please
              leave your contact details and our Grant Specialist will be in
              touch shortly to guide you through the application process.
            </p>
          </div>
          <div className="psg_form">
            <h2>Get a free demo</h2>
            <form onSubmit={this.handleSubmit}>
              <div className="form_input">
                <label>Name</label>
                <input
                  type="text"
                  value={this.state.name}
                  onChange={this.handleNameChange}
                  placeholder="Your first and last name"
                />
              </div>
              <div className="form_input">
                <label>Email Address</label>
                <input
                  type="email"
                  value={this.state.email}
                  onChange={this.handleEmailChange}
                  placeholder="Your email address"
                />
              </div>
              <div className="form_input">
                <label>Phone Number</label>
                <input
                  type="text"
                  value={this.state.phone}
                  onChange={this.handlePhoneChange}
                  placeholder="Your mobile phone number"
                />
              </div>
              <div className="form_input">
                <label>How many vehicles in yout fleet?</label>
                <select
                  value={this.state.vehicle}
                  onChange={this.handleVehicleChange}
                >
                  <option value="0">Select one..</option>
                  <option value="1">1</option>
                  <option value="2-5">2-5</option>
                  <option value="6-15">6-15</option>
                  <option value="16-25">16-25</option>
                  <option value="25+">25+</option>
                </select>
              </div>
              <button className="submit-btn" type="submit">
                Submit
              </button>
            </form>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default PsgGrant;
